# SokoFarm

SokoFarm is a console-based game where the player moves around a grid, trying to get all the seeds into the storages.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

What things you need to install the software and how to install them

- .NET  7 or higher

### Installing

A step by step series of examples that tell you how to get a development environment running

1. Clone the repository
2. Navigate to the project directory
3. Run `dotnet build` to build the project
4. Run `dotnet run` to start the game

## Built With

- [.NET Core](https://dotnet.microsoft.com/download)


If you like it give it a star ✨✨